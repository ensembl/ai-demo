import React from 'react';

import PropTypes from 'prop-types';
import {
  Box, Stack,
} from '@mui/material';

import EmptyLink from './shared/EmptyLink';

const SprintCard = ({ date }) => (
  <Stack>
    <Box marginBottom="6px">
      <EmptyLink type="jira" text="PYMT-153: Stripe integration including Apple Pay" />
    </Box>
    <Box display="flex">
      <Box marginRight="15px">
        Status:
        {' '}
        <span style={{ color: 'darkred', fontWeight: 'bold' }}>In progress</span>
      </Box>
      <Box marginRight="15px">
        Assignee:
        {' '}
        <EmptyLink text="Sammy" />
        {' '}
      </Box>
      <Box marginRight="15px">
        Sprint:
        {' '}
        <EmptyLink text={date} />
      </Box>
    </Box>
  </Stack>
);

SprintCard.propTypes = {
  date: PropTypes.string.isRequired,
};

export default SprintCard;
