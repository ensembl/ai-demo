import React, { useState } from 'react';

import ReactQuill from 'react-quill';
import {
  Box,
  Stack,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  Select,
  MenuItem,
  TextField,
  Snackbar,
  Alert,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

import 'react-quill/dist/quill.snow.css';
import Jira from './images/Jira.png';
import { descriptions } from './data/descriptions';

const IssueDescription = () => {
  const navigate = useNavigate();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [description, setDescription] = useState('');
  const [descriptionIndex, setDescriptionIndex] = useState(0);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [showNext, setShowNext] = useState(false);

  const generateDescription = () => {
    switch (descriptionIndex) {
      case 2:
        setDescriptionIndex(0);
        break;
      default:
        setDescriptionIndex(descriptionIndex + 1);
    }
    setDescription(descriptions[descriptionIndex]);
  };

  const onClose = () => {
    setSnackbarOpen(false);
    setShowNext(true);
  };

  const createIssue = () => {
    setSnackbarOpen(true);
    setDialogOpen(false);
  };

  const nextSlide = () => {
    navigate('/status-report');
  };

  return (
    <Box textAlign="left" padding="0 175px">
      <Box marginBottom="20px">
        <img
          height="40px"
          src={Jira}
          alt="Jira"
        />
      </Box>
      <Button
        variant="contained"
        onClick={() => { setDialogOpen(true); }}
        disableElevation
      >
        Create issue
      </Button>
      <Snackbar
        open={snackbarOpen}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      >
        <Alert
          severity="success"
          sx={{ width: '100%' }}
          onClose={onClose}
        >
          <b>View and configure app in dark mode</b>
          {' created'}
        </Alert>
      </Snackbar>
      <Dialog
        fullWidth
        maxWidth="md"
        open={dialogOpen}
        onClose={() => { setDialogOpen(false); }}
      >
        <DialogTitle
          style={{
            fontWeight: 'bold',
            color: '#172b4d',
          }}
        >
          Create issue
        </DialogTitle>
        <DialogContent>
          <Box
            style={{
              fontSize: '0.8rem',
              fontWeight: 'bold',
              color: '#5e6c84',
            }}
            marginBottom="5px"
          >
            Project
          </Box>
          <Box marginBottom="20px">
            <Select
              labelId="project-label"
              id="project-select"
              defaultValue="Payments (PYT)"
              style={{ height: '35px' }}
              disabled
            >
              <MenuItem value="Payments (PYT)">Payments (PYT)</MenuItem>
            </Select>
          </Box>
          <Box
            style={{
              fontSize: '0.8rem',
              fontWeight: 'bold',
              color: '#5e6c84',
            }}
            marginBottom="5px"
          >
            Issue type
          </Box>
          <Box marginBottom="20px">
            <Select
              labelId="issue-type-label"
              id="issue-type-select"
              defaultValue="Task"
              style={{ height: '35px' }}
              disabled
            >
              <MenuItem value="Task">Task</MenuItem>
            </Select>
          </Box>
          <Box
            style={{
              fontSize: '0.8rem',
              fontWeight: 'bold',
              color: '#5e6c84',
            }}
            marginBottom="5px"
          >
            Summary
          </Box>
          <Box marginBottom="20px">
            <TextField
              fullWidth
              value="View and configure app in dark mode"
              size="small"
            />
          </Box>
          <Box
            style={{
              fontSize: '0.8rem',
              fontWeight: 'bold',
              color: '#5e6c84',
            }}
            marginBottom="5px"
          >
            Description
          </Box>
          <Stack marginBottom="5px" direction="row" alignItems="center" spacing="10px">
            <Box>
              <Button
                onClick={generateDescription}
                variant="outlined"
                size="small"
                style={{ fontSize: '0.7rem' }}
              >
                Insert suggestion
              </Button>
            </Box>
            <Box
              style={{
                fontSize: '0.8rem',
                fontStyle: 'italic',
                color: 'black',
              }}
            >
              Based on summary and similar issues
            </Box>
          </Stack>
          <Box marginBottom="10px">
            <ReactQuill
              theme="snow"
              value={description}
            />
          </Box>
          <Stack direction="row" justifyContent="flex-end" spacing="5px">
            <Button
              size="small"
              variant="contained"
              onClick={createIssue}
              disableElevation
            >
              Create
            </Button>
            <Button size="small" onClick={() => setDialogOpen(false)}>Cancel</Button>
          </Stack>
        </DialogContent>
      </Dialog>
      {
        showNext && (
          <Box style={{ position: 'absolute', bottom: '3%' }}>
            <Button onClick={nextSlide}>Next idea</Button>
          </Box>
        )
      }
    </Box>
  );
};

export default IssueDescription;
