import React, { useState } from 'react';
import {
  Box, Button, Stack,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Conversation from './shared/Conversation';
import EmptyLink from './shared/EmptyLink';
import SlackCard from './shared/SlackCard';

import Sammy from './images/Sammy.png';
import Bot from './images/Bot.png';
import SprintCard from './SprintCard';

const Reminder = () => {
  const navigate = useNavigate();
  const [buttonState, setButtonState] = useState(1);

  const onNext = () => {
    setButtonState(buttonState + 1);
  };

  const nextSlide = () => {
    navigate('/status-and-meeting');
  };

  const [disableBump, setDisabledBump] = useState(false);

  const onBump = () => {
    setDisabledBump(true);
    setTimeout(() => {
      setButtonState(buttonState + 1);
    }, 500);
  };

  const conversations = [
    {
      id: 1,
      image: Bot,
      name: 'Acme Bot',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <span>Hey
          {' '}
          <EmptyLink text="@Sammy:" />
          {' '}
          Do you think the Stripe feature will be finished in time for this sprint?
          Thanksgiving break is coming up soon.
        </span>,
    },
    {
      id: 2,
      image: Sammy,
      name: 'Sammy',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <span>
          {`I'm stuck on this payment module integration that's really annoying.
          I'm going to ping Oli tomorrow morning if I'm still stuck by then.
          But probably won't be finished in time.`}
        </span>,
    },
    {
      id: 3,
      image: Bot,
      name: 'Acme Bot',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <div>
          <span>
            Thanks for the update
            {' '}
            <EmptyLink text="@Sammy" />
            {'. '}
            I&apos;ve left a comment in
            {' '}
            <EmptyLink type="jira" text="PYMT-153." />
            {' '}
            <EmptyLink text="@Jane:" />
            {' '}
            Do you want to bump it to the next sprint?
          </span>
          <SlackCard content={<SprintCard date="Nov 29 week" />} />
          <Button variant="outlined" disabled={disableBump} onClick={onBump}>
            Bump it to Dec 6 week sprint
          </Button>
        </div>,
    },
    {
      id: 4,
      image: Bot,
      name: 'Acme Bot',
      time: 'A moment ago',
      content:
        // eslint-disable-next-line
        <div>
          <span>
            {'Got it.  I\'ve bumped the issue to next week.'}
          </span>
          <SlackCard content={<SprintCard date="Dec 6 week" />} />
        </div>,
    },
  ];

  return (
    <>
      <Stack direction="column" alignItems="center">
        {conversations.map((conversation) => (
          buttonState >= conversation.id && (
            <Conversation
              key={conversation.id}
              image={conversation.image}
              title={conversation.name}
              subheader={conversation.time}
              content={conversation.content}
            />
          )
        ))}
      </Stack>
      <Box>
        {buttonState !== 3 && buttonState < 4 && <Button onClick={onNext}>Next</Button>}
        {buttonState >= 4 && <Button onClick={nextSlide}>Next idea</Button>}
      </Box>
    </>
  );
};

export default Reminder;
