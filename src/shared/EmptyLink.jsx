import React from 'react';
import {
  Link,
} from '@mui/material';
import PropTypes from 'prop-types';

const EmptyLink = ({ text, type }) => {
  let href;
  if (type === 'jira') {
    href = 'https://ensembl.atlassian.net/jira/software/projects/AD/boards/1?selectedIssue=AD-34';
  }

  return (
    <>
      {type && (
        <Link
          href={href}
          target="_blank"
          rel="noreferrer"
          underline="none"
        >
          {text}
        </Link>
      )}
      {!type && (
        <span style={{ color: '#1976d2', cursor: 'pointer' }}>{text}</span>
      )}
    </>
  );
};

EmptyLink.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
};

EmptyLink.defaultProps = {
  type: null,
};

export default EmptyLink;
