import React from 'react';

import PropTypes from 'prop-types';

import {
  Checkbox, Stack, Box,
  FormControl, FormGroup, FormLabel, FormHelperText, FormControlLabel,
} from '@mui/material';

const IssuesCheckboxes = ({
  issues, setChecked, formLabel, helperText, checkedState,
}) => {
  const onChange = (event) => {
    setChecked({
      ...checkedState,
      [event.target.name]: event.target.checked,
    });
  };

  return (
    <Stack display="flex" justifyContent="flex-start">
      <FormControl component="fieldset" variant="standard">
        <FormLabel
          style={{
            textAlign: 'left', color: 'black', fontSize: '1rem', fontWeight: 'bold',
          }}
          component="legend"
        >
          {formLabel}
        </FormLabel>
        {helperText && <FormHelperText style={{ fontSize: '0.9em' }}>{helperText}</FormHelperText>}
        <Stack display="flex" justifyContent="flex-start">
          <FormGroup style={{ marginLeft: '8px' }}>
            {issues.map((issue) => (
              <Box key={issue.id} style={{ marginBottom: '8px' }}>
                <FormControlLabel
                  control={(
                    <Box>
                      <Checkbox
                        checked={checkedState[issue.name]}
                        onChange={onChange}
                        name={issue.name}
                        sx={{ '& .MuiSvgIcon-root': { fontSize: '1.2rem' }, height: '30px' }}
                      />
                    </Box>
                  )}
                  label={issue.name}
                />
              </Box>
            ))}
          </FormGroup>
        </Stack>
      </FormControl>
    </Stack>
  );
};

IssuesCheckboxes.propTypes = {
  issues: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)).isRequired,
  formLabel: PropTypes.string.isRequired,
  setChecked: PropTypes.func.isRequired,
  helperText: PropTypes.string,
  checkedState: PropTypes.objectOf(PropTypes.any).isRequired,
};

IssuesCheckboxes.defaultProps = {
  helperText: null,
};

export default IssuesCheckboxes;
