import React from 'react';

import PropTypes from 'prop-types';
import {
  Box,
} from '@mui/material';

const SlackCard = ({ content }) => (
  <Box
    display="flex"
    style={{
      width: '650px',
      backgroundColor: 'lightgrey',
      margin: '10px 0',
      padding: '15px',
    }}
  >
    <div style={{
      width: '4px',
      marginRight: '10px',
      backgroundColor: 'grey',
    }}
    />
    <div style={{ width: '100%' }}>{content}</div>
  </Box>
);

SlackCard.propTypes = {
  content: PropTypes.node.isRequired,
};

export default SlackCard;
