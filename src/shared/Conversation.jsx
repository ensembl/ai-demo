import React from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Stack,
  Avatar,
} from '@mui/material';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';

const Conversation = ({
  image, title, subheader, content, visibleToYou,
}) => (
  <Box style={{ width: '900px' }}>
    {visibleToYou && (
      <Box style={{ margin: '13px 20px 5px 20px' }} display="flex" alignItems="center">
        <VisibilityOutlinedIcon style={{ fill: 'grey', marginRight: '5px' }} />
        <span style={{ color: 'grey' }}>Only visible to you</span>
      </Box>
    )}
    <Stack direction="row">
      <Box display="flex">
        <Avatar sx={{ width: 60, height: 60 }} src={image} alt="Sammy" />
      </Box>
      <Stack marginLeft="10px" marginBottom="30px">

        <Box display="flex">
          <span style={{ display: 'inline-block', marginRight: '10px', fontWeight: 'bold' }}>{title}</span>
          <span style={{ color: 'grey' }}>{subheader}</span>
        </Box>
        <Box display="flex" textAlign="left">
          {content}
        </Box>
      </Stack>
    </Stack>
  </Box>
);

Conversation.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subheader: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired,
  visibleToYou: PropTypes.bool,
};

Conversation.defaultProps = {
  visibleToYou: false,
};

export default Conversation;
