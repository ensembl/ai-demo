import React, { useState } from 'react';

import PropTypes from 'prop-types';

import {
  Box, Button, Card, TextField, Stack, FormControl,
} from '@mui/material';

import { PASSWORD } from './config';

const Login = ({ setLogin, setUserName }) => {
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState();
  const onClick = () => {
    setLoading(true);
    setTimeout(() => {
      if (password === PASSWORD) {
        setLogin(true);
      } else {
        setLoading(false);
      }
    }, 1000);
  };

  return (
    <Box display="flex" justifyContent="center" alignItem="center" marginTop="20%">
      <Card
        variant="outlined"
        style={{
          padding: '15px 20px',
          backgroundColor: '#f0f0f0',
          lineHeight: '1.3rem',
        }}
      >
        <Stack direction="column">
          <FormControl>
            <TextField
              onChange={(e) => { setUserName(e.target.value); }}
              placeholder="User name"
            />
            <TextField
              onChange={(e) => { setPassword(e.target.value); }}
              placeholder="Password"
              type="password"
            />
            <Button
              disabled={loading}
              onClick={onClick}
            >
              Login
            </Button>
          </FormControl>
        </Stack>
      </Card>
    </Box>
  );
};

Login.propTypes = {
  setLogin: PropTypes.func.isRequired,
  setUserName: PropTypes.func.isRequired,
};

export default Login;
