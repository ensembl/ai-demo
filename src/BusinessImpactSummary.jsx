import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import axios from 'axios';
import pluralize from 'pluralize';
import {
  Box, Button, Stack, Select, MenuItem, FormControl, Tooltip, CircularProgress, Card,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

import IssuesCheckboxes from './shared/IssuesCheckboxes';

import { issues } from './data/issues';
import { OPENAI_KEY } from './config';
import { getHighlightConfig, getWinsConfig, getProblemsConfig } from './data/openai';

const BusinessImpactSummary = ({ openAI }) => {
  const navigate = useNavigate();
  const [showReport, setShowReport] = useState(false);
  const [sprint, setSprint] = useState('');
  const [copiedSummary, setCopiedSummary] = useState(false);
  const [fetchingOpenAI, setFetchingOpenAI] = useState(false);

  const [highlightSummary, setHighlightSummary] = useState();
  const [winSummary, setWinSummary] = useState();
  const [problemSummary, setProblemSummary] = useState();
  const [uniqueIssues, setUniqueIssues] = useState(0);

  const [loadingState, setLoadingState] = useState(0);

  const highlights = issues.filter((issue) => 'highlight' in issue);
  const wins = issues.filter((issue) => 'win' in issue);
  const problems = issues.filter((issue) => 'problem' in issue);

  const [highlightState, setHighlightState] = useState(
    highlights.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
  );
  const [winState, setWinState] = useState(
    wins.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
  );
  const [problemState, setProblemState] = useState(
    problems.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
  );

  const [disableGenerateReport, setDisableGenerateReport] = useState(true);

  useEffect(() => {
    const highlightSelected = Object.values(highlightState).filter((value) => value).length;
    const winSelected = Object.values(
      winState,
    ).filter((value) => value).length;
    const problemSelected = Object.values(
      problemState,
    ).filter((value) => value).length;
    if (highlightSelected >= 1 && winSelected >= 1
      && highlightSelected < 4 && winSelected < 4
      && problemSelected < 4) {
      setDisableGenerateReport(false);
    } else {
      setDisableGenerateReport(true);
    }
  }, [highlightState, winState, problemState]);

  const shuffle = (array) => {
    // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    for (let i = array.length - 1; i > 0; i -= 1) {
      const j = Math.floor(Math.random() * (i + 1));
      // eslint-disable-next-line
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  };

  const highlightEmojis = ['🚀', '🏎️', '📺'];
  const winsEmojis = ['🎉', '🥇', '🥳'];
  const problemsEmojis = ['🚧', '🤔', '😮'];

  const highlightLabels = ['Usage/revenue', 'Customer support', 'Product roadmap'];
  const winsLabels = ['User requested', 'UI/UX', 'Unblocked something'];
  const problemsLabels = ['Delayed', 'Scope', 'Tech challenge'];

  const [highlightEmoji, setHighlightEmoji] = useState(
    highlightEmojis[Math.floor(Math.random() * highlightEmojis.length)],
  );
  const [winsEmoji, setWinEmoji] = useState(
    winsEmojis[Math.floor(Math.random() * winsEmojis.length)],
  );
  const [problemsEmoji, setProblemsEmoji] = useState(
    problemsEmojis[Math.floor(Math.random() * problemsEmojis.length)],
  );

  const randomizeEmoji = () => {
    setHighlightEmoji(highlightEmojis[Math.floor(Math.random() * highlightEmojis.length)]);
    setWinEmoji(winsEmojis[Math.floor(Math.random() * winsEmojis.length)]);
    setProblemsEmoji(problemsEmojis[Math.floor(Math.random() * problemsEmojis.length)]);
  };

  const updateUniqueIssues = () => {
    const selectedHighlights = Object.keys(highlightState).filter(
      (key) => highlightState[key] !== false,
    );
    const selectedWins = Object.keys(winState).filter(
      (key) => winState[key] !== false,
    );
    const selectedProblems = Object.keys(problemState).filter(
      (key) => problemState[key] !== false,
    );
    const uniqueSelectedIssues = new Set(
      [...selectedHighlights, ...selectedWins, ...selectedProblems],
    );
    setUniqueIssues(uniqueSelectedIssues.size);
  };

  useEffect(() => {
    updateUniqueIssues();
    setShowReport(false);
    setCopiedSummary(false);
  }, [highlightState, winState, problemState]);

  useEffect(() => {
    shuffle(issues);
    setHighlightState(
      highlights.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
    );
    setWinState(
      wins.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
    );
    setProblemState(
      problems.reduce((a, v) => ({ ...a, [v.name]: false }), {}),
    );
  }, [sprint]);

  const nextSlide = () => {
    navigate('/issue-description');
  };

  const selectSprint = (event) => {
    setSprint(event.target.value);
  };

  const openAiCall = async ({ data }) => {
    console.info('Calling OpenAI API'); // make sure we are only making calls when needed because billing is by call
    const response = await axios({
      url: 'https://api.openai.com/v1/engines/davinci/completions',
      headers: {
        Authorization: `Bearer ${OPENAI_KEY}`,
        'Content-Type': 'application/json',
      },
      data,
      method: 'post',
    });
    return response;
  };

  const getAiHighlights = async () => {
    const selectedHighlights = Object.keys(highlightState).filter(
      (key) => highlightState[key] === true,
    );
    const aiHighlights = [];
    await Promise.all(selectedHighlights.map(async (issue) => {
      const response = await openAiCall(
        { data: getHighlightConfig({ issueTitle: issue.split(' ')[1] }) },
      );
      aiHighlights.push(response.data.choices[0].text);
    }));

    setHighlightSummary(aiHighlights.join(' '));
  };

  const getAiWins = async () => {
    const selectedWins = Object.keys(winState).filter(
      (key) => winState[key] === true,
    );
    const aiWins = [];
    await Promise.all(selectedWins.map(async (issue) => {
      const response = await openAiCall(
        { data: getWinsConfig({ issueTitle: issue.split(' ')[1] }) },
      );
      aiWins.push(<li key={response.data.id}>{response.data.choices[0].text}</li>);
    }));
    setWinSummary(aiWins);
  };

  const getAiProblems = async () => {
    const selectedProblems = Object.keys(problemState).filter(
      (key) => problemState[key] === true,
    );
    const aiProblems = [];
    await Promise.all(selectedProblems.map(async (issue) => {
      const response = await openAiCall(
        { data: getProblemsConfig({ issueTitle: issue.split(' ')[1] }) },
      );
      aiProblems.push(<li key={response.data.id}>{response.data.choices[0].text}</li>);
    }));
    setProblemSummary(aiProblems);
  };

  const generateReport = async () => {
    setDisableGenerateReport(true);
    if (openAI) {
      setFetchingOpenAI(true);
      setLoadingState(1);
      await Promise.all([
        getAiHighlights(),
        getAiWins(),
        getAiProblems(),
      ]);
      setFetchingOpenAI(false);
    } else {
      setHighlightSummary(
        shuffle(issues.filter(
          (issue) => highlightState[issue.name],
        )).map((issue) => issue.highlight.boring[
          Math.floor(Math.random() * issue.highlight.boring.length)]),
      );
      setWinSummary(
        shuffle(issues.filter(
          (issue) => winState[issue.name],
        )).map((issue) => issue.win.boring[
          Math.floor(Math.random() * issue.win.boring.length)]),
      );

      setProblemSummary(
        shuffle(issues.filter(
          (issue) => problemState[issue.name],
        )).map((issue) => (issue.problem.boring[
          Math.floor(Math.random() * issue.problem.boring.length)])),
      );
    }
  };

  const generateNewReport = async () => {
    setShowReport(false);
    setDisableGenerateReport(true);
    randomizeEmoji();
    setLoadingState(1);
    await generateReport();
  };

  const copySummary = async () => {
    navigator.clipboard.writeText(document.querySelector('#report').innerText);
    setCopiedSummary(true);
  };

  useEffect(() => {
    if (!openAI) {
      if (loadingState === 1) {
        setTimeout(() => {
          setLoadingState(loadingState + 1);
        }, 1500);
      }
      if (loadingState === 2) {
        setTimeout(() => {
          setLoadingState(0);
          setShowReport(true);
          setCopiedSummary(false);
          setDisableGenerateReport(false);
        }, 1500);
      }
    }
  }, [loadingState]);

  useEffect(() => {
    if (openAI) {
      if (loadingState === 1 && fetchingOpenAI === false) {
        setLoadingState(0);
        setShowReport(true);
        setCopiedSummary(false);
        setDisableGenerateReport(false);
      }
    }
  }, [fetchingOpenAI]);

  const viewSummary = () => {
    if (showReport) {
      document.querySelector('#report').scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

  const writeADifferentOne = () => {
    randomizeEmoji();
    setCopiedSummary(false);
    generateReport();
  };

  return (
    <Box textAlign="left" marginBottom="300px" padding="0 175px">
      <Box marginBottom="20px" fontWeight="bold" fontSize="2rem">
        Create sprint business impact summary with AI 🤖
      </Box>
      <Box marginBottom="20px">
        <FormControl size="small">
          <Select displayEmpty value={sprint} onChange={selectSprint}>
            <MenuItem sx={{ fontSize: '0.9rem' }} disabled value="">Pick Jira sprint</MenuItem>
            <MenuItem sx={{ fontSize: '0.9rem' }} value={1}>2021 Nov 22 week</MenuItem>
            <MenuItem sx={{ fontSize: '0.9rem' }} value={2}>2021 Nov 29 week</MenuItem>
            <MenuItem sx={{ fontSize: '0.9rem' }} value={3}>2021 Dec 6 week</MenuItem>
          </Select>
        </FormControl>
      </Box>

      {sprint && (
        <Stack direction="column" justifyContent="flex-start">
          <Stack direction="column" justifyContent="flex-start">
            <Box marginBottom="20px">
              <IssuesCheckboxes
                formLabel="Pick 1 to 3 issues that were top priority 🦄"
                issues={highlights}
                checkedState={highlightState}
                setChecked={setHighlightState}
                labels={highlightLabels}
              />
            </Box>
            <Box marginBottom="20px">
              <IssuesCheckboxes
                formLabel="Pick 1 to 3 issues that made the most positive impact 🥇"
                issues={wins}
                checkedState={winState}
                setChecked={setWinState}
                labels={winsLabels}
              />
            </Box>
            <Box marginBottom="20px">
              <IssuesCheckboxes
                formLabel="Pick up to 3 issues that had any problems 🤕 (optional)"
                issues={problems}
                checkedState={problemState}
                setChecked={setProblemState}
                helperText="Had implementation difficulties, are delayed/postponed, or otherwise have
                ongoing business risks"
                labels={problemsLabels}
              />
            </Box>
          </Stack>
          <Stack direction="row">
            <Button
              disabled={disableGenerateReport}
              disableElevation
              variant="contained"
              onClick={generateNewReport}
              style={{ marginRight: '20px' }}
            >
              Create summary
            </Button>
            {loadingState !== 0 && (
              <Stack direction="row" alignItems="center" justifyContent="center">
                <CircularProgress size="0.9rem" style={{ marginRight: '8px', color: 'black' }} />
                <Box fontSize="0.9rem">
                  {loadingState === 1 ? '🔎 Analyzing Jira issues and comments...' : '🤖 Writing summary with AI...'}
                </Box>
              </Stack>
            )}
            {loadingState === 0 && showReport && fetchingOpenAI === false && (
              <Button onClick={viewSummary}>View summary</Button>
            )}
          </Stack>

        </Stack>
      )}
      {
        showReport && fetchingOpenAI === false && (
          <>
            <Box marginTop="20px" fontStyle="italic" fontSize=".9rem">
              {`Created summary based on ${uniqueIssues} issue ${pluralize('title', uniqueIssues)}`}
            </Box>
            <Box
              id="report"
              marginTop="20px"
              marginBottom="20px"
              fontSize="1.5rem"
              fontWeight="bold"
            >
              Business impact summary
            </Box>
            <Card
              variant="outlined"
              style={{
                padding: '15px 20px',
                backgroundColor: '#f0f0f0',
                lineHeight: '1.3rem',
              }}
            >
              <>
                <h3 style={{ margin: '0 0 10px 0' }}>{`${highlightEmoji} Highlight`}</h3>
                {highlightSummary}
              </>
              <Box marginTop="20px">
                <h3 style={{ margin: '0 0 10px 0' }}>{`${winsEmoji} Wins`}</h3>
                <ul>
                  {winSummary}
                </ul>
              </Box>
              {problemSummary.length !== 0 && (
                <Box marginTop="20px">
                  <h3 style={{ margin: '0 0 10px 0' }}>{`${problemsEmoji} Outstanding risks`}</h3>
                  <ul>
                    {problemSummary}
                  </ul>
                </Box>
              )}
              <Stack direction="row" justifyContent="flex-start" marginTop="10px">
                <Button
                  style={{ marginRight: '10px' }}
                  variant="contained"
                  disableElevation
                  onClick={writeADifferentOne}
                  size="small"
                >
                  Write a different one
                </Button>
                <Tooltip title={!copiedSummary ? 'Copy to clipboard' : 'Copied 😀'} placement="top">
                  <Button
                    variant="outlined"
                    onClick={copySummary}
                    size="small"
                    style={{ backgroundColor: 'white' }}
                  >
                    Copy summary
                  </Button>
                </Tooltip>
              </Stack>
            </Card>
            {showReport && (
              <Box marginTop="60px">
                <Button onClick={nextSlide}>Next idea</Button>
              </Box>
            )}
          </>
        )
      }
    </Box>
  );
};

BusinessImpactSummary.propTypes = {
  openAI: PropTypes.bool,
};

BusinessImpactSummary.defaultProps = {
  openAI: false,
};

export default BusinessImpactSummary;
