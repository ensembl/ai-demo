import axios from 'axios';
import { OPENAI_KEY } from './config';

const openAiCall = async ({ data, endpoint }) => {
  console.info('Calling OpenAI API'); // make sure we are only making calls when needed because billing is by call
  const response = await axios({
    url: `https://api.openai.com/v1/engines/${endpoint}/completions`,
    headers: {
      Authorization: `Bearer ${OPENAI_KEY}`,
      'Content-Type': 'application/json',
    },
    data,
    method: 'post',
  });
  return response;
};

const getConfig = ({
  title, description, n,
}) => ({
  prompt: `
  This is a business report writer:
  ###
  User story: Paypal payment integration
  Description: Was a critical feature this quarter
  Two-sentence report: The Paypal payment integration feature was finished this sprint. It was an important feature this quarter to further our vision of reducing friction in our payments flow by offering more options for customers.
  ###
  User story: Design updates to landing page
  Description: Improvement to marketing and our brand
  Two-sentence report: We finished the design updates to landing page work this sprint. We worked closely with the marketing team on it and they think it will significantly improve our inbound funnel metrics.
  ###
  User story: Simplifying menu UI
  Description: Fixed user confusion
  Two-sentence report: The old menu UI was causing a lot of confusion. Based on several user research sessions, the new version is a lot easier to use and will reduce a lot of user pain, especially reducing user support requests.
  ###
  User story: ${title}
  Description: ${description}.
  Two-sentence report:`,
  max_tokens: 150,
  temperature: 1,
  top_p: 0.4,
  presence_penalty: 2,
  frequency_penalty: 2,
  stop: '###',
  n,
});

export { openAiCall, getConfig };
