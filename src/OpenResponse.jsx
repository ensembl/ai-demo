import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import {
  Box, FormControl, Checkbox, FormControlLabel, RadioGroup, Radio,
  Stack, Button, Card, TextField, Tooltip, Autocomplete,
} from '@mui/material';
import papa from 'papaparse';
import faker from 'faker';
import ReactQuill from 'react-quill';

import { openAiCall, getConfig } from './util';
import './OpenResponse.css';

import SlackImage from './images/Slack_RGB.png';

const fakeFirstNames = Array(5)
  .fill(null)
  .map(() => faker.name.firstName());
const fakeEmails = Array(5)
  .fill(null)
  .map((v, i) => faker.internet.email(fakeFirstNames[i], faker.name.lastName(), 'ensembl.ai'));

const channels = ['#WeeklyUpdate', '#ProductTeam', '#DevTeam'];

const OpenResponse = ({ userName }) => {
  const placeholders = [
    'e.g.: Was a critical feature this quarter',
    'e.g.: Improvement to marketing and our brand',
    'e.g.: Fixed user confusion',
    'e.g.: Fixed security risk',
    'e.g.: Will streamline product engineering handoff',
    'e.g.: Was a frequently requested feature',
    'e.g.: Will triple our inbound leads',
    'e.g.: Will improve usage by 50%',
  ];

  const [issueState, setIssueState] = useState();
  const [importedIssueState, setImportedIssueState] = useState();
  const [reportChannel, setReportChannel] = useState('');
  const [showReport, setShowReport] = useState(false);
  const [reportContent, setReportContent] = useState('');
  const [sending, setSending] = useState(false);
  const [emails, setEmails] = useState([fakeEmails[0], fakeEmails[1]]);
  const [sent, setSent] = useState(false);
  const [slack, setSlack] = useState({
    '#WeeklyUpdate': false,
    '#ProductTeam': false,
    '#DevTeam': false,
  });

  const onCheckboxChange = (event) => {
    setIssueState({
      ...issueState,
      [event.target.name]: {
        ...issueState[event.target.name],
        value: event.target.checked,
      },
    });
  };

  // https://stackoverflow.com/questions/61419710/how-to-import-a-csv-file-in-reactjs
  const fetchCsv = async () => {
    const response = await fetch('data/Jira.csv');
    const reader = response.body.getReader();
    const result = await reader.read();
    const decoder = new TextDecoder('utf-8');
    const csv = await decoder.decode(result.value);
    return csv;
  };

  useEffect(async () => {
    const data = papa.parse(await fetchCsv(), { header: true });
    setImportedIssueState(data);
  }, []);

  useEffect(() => {
    if (importedIssueState) {
      const first15Issues = importedIssueState.data.slice(0, 15);
      const formattedIssues = first15Issues.reduce((a, c, i) => ({
        ...a,
        [c.Summary]: {
          placeholder: placeholders[i % placeholders.length],
          value: false,
        },
      }), {});
      setIssueState(formattedIssues);
    }
  }, [importedIssueState]);

  const onTextChange = (event, issueName) => {
    setIssueState({
      ...issueState,
      [issueName]: {
        ...issueState[issueName],
        value: event.target.value,
        options: {},
        selected: 'none',
      },
    });
  };

  const selectReportChannel = (event) => {
    setReportChannel(event.target.value);
  };

  const onGenerateOptions = async (event) => {
    const issueName = event.target.value;
    setIssueState({
      ...issueState,
      [issueName]: {
        ...issueState[issueName],
        loading: true,
      },
    });

    const res = await openAiCall(
      {
        data: getConfig(
          {
            title: issueName,
            description: issueState[issueName].value,
            n: 3,
            reportChannel,
          },
        ),
        endpoint: 'davinci',
      },
    );

    // const res = {
    //   data: {
    //     choices: [
    //       { text: 'hello' },
    //       { text: 'hello' },
    //       { text: 'hello' },
    //     ],
    //   },
    // };

    const options = {};
    res.data.choices.forEach((choice, i) => {
      options[i] = choice.text.trim();
    });
    setIssueState({
      ...issueState,
      [issueName]: {
        ...issueState[issueName],
        options: {
          ...issueState[issueName].options,
          ...options,
        },
        selected: '',
      },
    });
  };

  const onOptionSelected = (index, issueName) => {
    setIssueState({
      ...issueState,
      [issueName]: {
        ...issueState[issueName],
        selected: index,
      },
    });
  };

  const onOptionTextChange = (index, issueName, text) => {
    setIssueState({
      ...issueState,
      [issueName]: {
        ...issueState[issueName],
        options: {
          ...issueState[issueName].options,
          [index]: text,
        },
      },
    });
  };

  const onEmailChange = (event, newValue) => {
    setEmails(newValue);
  };

  const onSendButton = () => {
    setSending(true);
    setTimeout(() => {
      setSending(false);
      setSent(true);
    }, 0);
  };

  const onSlack = (e) => {
    setSending(true);
    setTimeout(() => {
      setSlack({
        ...slack,
        [e.target.value]: true,
      });
      setSending(false);
    }, 0);
  };

  const getHelpText = (issueName) => {
    const { length } = issueState[issueName].value;
    if (length === 0) { return 'Please enter a the business impact'; }
    if (length < 20) { return 'Enter short phrases to describe the business impact.'; }
    if (length < 60) { return 'Great, we have enough for the AI!'; }
    if (length >= 60) {
      return "Don't worry about writing too complete sentence, just the main impact.";
    }
    return '';
  };

  const getHelpTextColor = (length) => {
    if (length < 20) return 'grey';
    if (length < 60) return 'green';
    return 'red';
  };

  const onCompileReport = () => {
    setShowReport(true);
    const selectedIssues = Object.entries(issueState)
      .reduce((p, [title, value]) => {
        if (value.selected) {
          return { ...p, [title]: value };
        }
        return p;
      }, {});
    const items = Object.entries(selectedIssues).map(([title, value]) => `<li><b>${title}:</b> ${value.options[value.selected]}</li>`).join(' ');
    const content = reportChannel === 'email'
      ? `
    Hi ${fakeFirstNames[0]} and ${fakeFirstNames[1]},<br><br>
    Please see the following Q1 update.
    <ul>
      ${items}
    </ul><br>
    Thanks,<br>
    ${userName || 'First name'}
    ` : `
      Hey team! Here are the Q1 updates.<br><br>
      <ul>
      ${items}
      </ul><br>
    `;
    setReportContent(content);
  };

  return (
    <Box>
      <Box textAlign="left" marginBottom="300px" padding="0 175px">
        <Box marginBottom="20px" fontWeight="bold" fontSize="2rem">
          Create business impact report with AI 🤖
        </Box>
        <div className="reportChannel">
          <Box>
            <h4>
              Where are you sending this report?
            </h4>
          </Box>
          <FormControl>
            <RadioGroup
              row
              aria-labelledby="radio-buttons-group-label"
              name="radio-buttons-group"
              value={reportChannel}
              onChange={selectReportChannel}
            >
              <FormControlLabel value="email" control={<Radio />} label="Email" />
              <FormControlLabel value="slack" control={<Radio />} label="Slack" />
            </RadioGroup>
          </FormControl>
        </div>
        {reportChannel && (
          <div className="issues">
            <Box>
              <h4>
                Select 2-3 issues to include in the report.
                For each, briefly describe the business impact.
              </h4>
            </Box>
            <FormControl>
              {issueState && Object.entries(issueState).map(([issueName, issue]) => (
                <Card
                  style={{
                    padding: '10px',
                    lineHeight: '1.3rem',
                    marginTop: '10px',
                    width: '800px',
                    border: `${issueState[issueName].value ? '2px solid #2e7d32' : ''}`,
                  }}
                  key={issueName}
                  variant="outlined"
                >
                  <Stack justifyContent="center">
                    <FormControlLabel
                      control={(
                        <Checkbox
                          checked={issueState[issueName].value !== false}
                          onChange={onCheckboxChange}
                          name={issueName}
                        />
                      )}
                      label={issueName}
                      id={issue.id}
                    />
                    {issueState[issueName].value !== false
                      && (
                        <Box marginLeft="30px">
                          <Stack
                            direction="row"
                            justifyContent="space-between"
                            alignItems="flex-start"
                          >
                            <Stack direction="column">
                              <TextField
                                style={{ width: '550px' }}
                                onChange={(event) => onTextChange(event, issueName)}
                                placeholder={issueState[issueName].placeholder}
                                size="small"
                                required
                                id={issueName}
                                error={issueState[issueName].value.length === 0}
                                helperText={getHelpText(issueName)}
                                sx={
                                  { '& .MuiFormHelperText-root': { color: getHelpTextColor(issueState[issueName].value.length), fontSize: '0.6rem' } }
                                }
                              />
                            </Stack>
                            <Box>
                              <Button
                                onClick={onGenerateOptions}
                                variant="outlined"
                                value={issueName}
                                style={{ height: '2.5rem' }}
                                disabled={issueState[issueName].loading}
                              >
                                {!issueState[issueName].loading ? 'Generate options with AI' : 'Generating...'}
                              </Button>
                            </Box>

                          </Stack>
                          <Stack direction="column" justifyContent="center">
                            {issueState[issueName].options
                              && Object.entries(issueState[issueName].options).map(
                                ([index, value]) => (
                                  <Box key={index} marginTop="10px">
                                    <RadioGroup
                                      aria-labelledby="controlled-radio-buttons-group"
                                      name="controlled-radio-buttons-group"
                                      value={issueState[issueName].selected}
                                      onChange={() => onOptionSelected(index, issueName)}
                                    >
                                      <Stack
                                        direction="row"
                                        justifyContent="space-between"
                                        alignItems="space-between"
                                      >
                                        <FormControlLabel
                                          control={<Radio />}
                                          value={index}
                                          label={
                                            issueState[issueName].selected === index
                                              ? (
                                                <TextField
                                                  style={{ width: '730px' }}
                                                  multiline
                                                  required
                                                  value={value}
                                                  disabled={
                                                    issueState[issueName].selected !== index
                                                  }
                                                  onChange={
                                                    (e) => onOptionTextChange(
                                                      index,
                                                      issueName,
                                                      e.target.value,
                                                    )
                                                  }
                                                  sx={
                                                    {
                                                      '& .MuiOutlinedInput-input': { fontSize: '0.8rem' },
                                                      '& textarea.MuiOutlinedInput-input.MuiOutlinedInput-input.Mui-disabled': { '-webkit-text-fill-color': 'black' },
                                                    }
                                                  }
                                                />
                                              )
                                              : (
                                                <div style={{ fontSize: '0.8rem', margin: '20px 0' }}>
                                                  {value}
                                                </div>
                                              )

                                          }
                                        />
                                      </Stack>
                                    </RadioGroup>
                                  </Box>
                                ),
                              )}
                          </Stack>
                        </Box>
                      )}
                  </Stack>
                </Card>
              ))}
            </FormControl>
          </div>
        )}
        {reportChannel && (
          <Box margin="30px 0" width="820px">
            <Button
              fullWidth
              variant="contained"
              color="success"
              onClick={onCompileReport}
            >
              Compile report
            </Button>
          </Box>
        )}
        {showReport
          && (
            <Box width="820px">
              <ReactQuill
                theme="snow"
                value={reportContent}
              />
              <Box marginTop="10px" marginBottom="10px">
                {reportChannel === 'email' && (
                  <>
                    <Autocomplete
                      multiple
                      freeSolo
                      options={fakeEmails}
                      id="tags-standard"
                      value={emails}
                      onChange={onEmailChange}
                      renderInput={(params) => (
                        <TextField
                          {...params} // eslint-disable-line
                          variant="standard"
                          label="Suggested email addresses"
                        />
                      )}
                    />
                    <Tooltip title={sent ? 'Email sent 😀' : ''}>
                      <span>
                        <Button
                          disabled={sending || sent}
                          onClick={onSendButton}
                        >
                          Send
                        </Button>
                      </span>
                    </Tooltip>
                  </>
                )}
                {reportChannel === 'slack' && (
                  <>
                    <Stack fontSize=".8rem" direction="row" alignItems="center">
                      <Box>Suggested channels</Box>
                      <img width="100px" src={SlackImage} alt="Slack" />
                    </Stack>
                    {channels.map((channel) => (
                      <Box
                        key={channel}
                        marginRight="5px"
                        display="inline-block"
                      >
                        <Tooltip
                          placement="bottom"
                          title={slack[channel] ? `Posted to ${channel} 😀` : ''}
                        >
                          <Box>
                            <Button
                              disabled={sending || slack[channel]}
                              onClick={onSlack}
                              size="small"
                              variant="outlined"
                              value={channel}
                            >
                              {channel}
                            </Button>
                          </Box>
                        </Tooltip>
                      </Box>
                    ))}
                  </>
                )}
              </Box>
            </Box>
          )}

      </Box>
    </Box>
  );
};

OpenResponse.propTypes = {
  userName: PropTypes.string.isRequired,
};

export default OpenResponse;
