const descriptions = [
  `
  <h3>Requirements</h3>
  <ul>
    <li>Existing users get light mode by default.</li>
    <li>New users get dark mode by default.</li>
    <li>View app using system setting.</li>
    <li>Default mode is light mode.</li>
    <li>View app in light mode or dark mode.</li>
  </ul>
  <br />
  <h3>UAT test cases</h3>
  <ul>
    <li>Given that I am a new user</li>
    <ul>
      <li>When I first log into the app.</li>
      <li>Then I view the app in dark mode.</li>
    </ul>
  </ul>
  `,
  `
  <h3>Specs</h3>
  <ul>
    <li>View app in light mode or dark mode.</li>
    <li>View app using system setting.</li>
    <li>Default mode is light mode.</li>
    <li>Existing users get light mode by default.</li>
    <li>New users get dark mode by default.</li>
  </ul>
  <br />
  <h3>Edge cases</h3>
  <ul>
    <li>Users created before January 13, 2015 only have light mode.</li>
  </ul>
  `,
  `
  <h3>Behavior / use cases</h3>
  <ul>
    <li>View app in light mode or dark mode.</li>
    <li>View app using system setting.</li>
    <li>Default mode is light mode.</li>
  </ul>
  <br />
  <h3>Existing users</h3>
  <ul>
    <li>Existing users get light mode by default.</li>
  </ul>
  <br />
  <h3>New users</h3>
  <ul>
    <li>New users get dark mode by default.</li>
  </ul>
  <br />
  <h3>Acceptance criteria</h3>
  <ul>
    <li>Given that I am a new user.</li>
    <ul>
      <li>When I first log into the app.</li>
      <li>Then I view the app in dark mode.</li>
    </ul>
  </ul>
  `,
];

export { descriptions };
