// https://beta.openai.com/docs/api-reference/completions
const getHighlightConfig = ({ issueTitle }) => ({
  prompt: `
    Topic: Shopping cart saving
    Two-Sentence Highlight: Shopping cart saving was shipped as the most critical feature in the sprint. It supports the upcoming ecommerce launch.
    ###
    Topic: API integration with internal system
    Two-Sentence Highlight: API integration with internal system will streamline the process between internal and customer database, and will radically enhance the collaboration across departments.
    ###
    Topic: ${issueTitle}
    Two-Sentence Highlight:`,
  max_tokens: 50,
  temperature: 0.7,
  stop: '###',
});

const getWinsConfig = ({ issueTitle }) => ({
  prompt: `
    Topic: Account info API call
    Two-Sentence Wins Summary: Account info API was shipped this sprint was a major blocker for shopping cart enhancements 🛒  and was also causing pain for the Customer Support team ☎️ since customers didn’t have self-service account access.
    ###
    Topic: Customer survey module
    Two-Setence Wins Summary: Customer survey module was finished this sprint and it allows us to quickly gather data on customer experience.  It's a huge win for improving our UI/UX design.
    ###
    Topic: ${issueTitle}
    Two-Sentence Wins Summary:`,
  max_tokens: 50,
  temperature: 0.5,
  stop: '###',
});

const getProblemsConfig = ({ issueTitle }) => ({
  prompt: `
    Topic: Database schema migration
    Two-Sentence Problem Summary: Database schema migration was a major blocker.  Migrating existing data to the new schema proof to be very challenging and will take more time.
    ###
    Topic: Newsletter email delivery
    Two-Setence Problem Summary: Newsletter email delivery is put on hold because of vendor related issues.  Will need to continue to follow up with vendor to figure out a viable solultion.
    ### 
    Topic: ${issueTitle}
    Two-Sentence Problem Summary:`,
  max_tokens: 50,
  temperature: 0.5,
  stop: '###',
});

export { getHighlightConfig, getWinsConfig, getProblemsConfig };
