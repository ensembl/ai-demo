/* eslint-disable */
import React from 'react';

import EmptyLink from '../shared/EmptyLink';

const issues = [
  {
    id: 32,
    name: 'STR-32 Shopping cart saving',
    highlight: {
      fun: [
        <span key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` was shipped 🚢 as the most critical feature in the sprint.
    It supports the upcoming ecommerce launch 🚀.`}
        </span>,
        <span key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` supports the upcoming ecommerce launch 🚀. It was completed 
      as the most critical feature 🏅 this sprint.`}
        </span>,
      ],
      boring: [
        <span key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` was shipped as the most critical feature in the sprint.
      It supports the upcoming ecommerce launch.`}
        </span>,
        <span key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` supports the upcoming ecommerce launch. It was completed 
        as the most critical feature this sprint.`}
        </span>,
      ]
    },
    win: {
      fun: [
        <li key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {`✨ is the marquee feature of the ecommerce launch. 
      Getting it done ahead of time this sprint mitigates delivery risk 🚚 
      and also allows business partners to have some breathing room for UAT.`}
        </li>,
        <li key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` is the topline feature 🔝 of the ecommerce launch. 
        We got it done ahead of time this sprint, mitigating a huge delivery risk. 
        Business partners 💼 can also have some breathing room for adequate UAT.`}
        </li>,
      ],
      boring: [
        <li key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` is the marquee feature of the ecommerce launch. 
      Getting it done ahead of time this sprint mitigates delivery risk 
      and also allows business partners to have some breathing room for UAT.`}
        </li>,
        <li key={'32'}>
          <EmptyLink type="jira" text=" Shopping cart saving" />
          {` is the topline feature of the ecommerce launch. 
        We got it done ahead of time this sprint, mitigating a huge delivery risk. 
        Business partners can also have some breathing room for adequate UAT.`}
        </li>,
      ]
    },
  },
  {
    id: 52,
    name: 'STR-52 Authenticate to payments server',
    highlight: {
      fun: [
        <span key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` 💸 is a top priority for the upcoming launch. 
        It was shipped in this sprint and so all technical implementation 
        risks for the `}
          <EmptyLink type="jira" text="quarterly roadmap" />
          {` are now mitigated. ✅`}
        </span>,
        <span key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` 💸 was completed this sprint and so now all technical implementation
         risks are mitigated this quarter. 
         It is a top priority for the upcoming launch 🚀.`}
        </span>,
      ],
      boring: [
        <span key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` is a top priority for the upcoming launch. 
        It was shipped in this sprint and so all technical implementation 
        risks for the `}
          <EmptyLink type="jira" text="quarterly roadmap" />
          {` are now mitigated.`}
        </span>,
        <span key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` was completed this sprint and so now all technical implementation
         risks are mitigated this quarter. 
         It is a top priority for the upcoming launch.`}
        </span>,
      ]
    },
    win: {
      fun: [
        <li key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` shipped this sprint was the final piece 🍰 of validating the payment flow. 
        The technical risk 💻 is now mitigated and the team can now tackle the product 
        concerns more directly.`}
        </li>,
        <li key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` 🤝🏿 shipped this sprint means the team can focus on product concerns, 
        since the the technical risk is now eliminated ✅.`}
        </li>,
      ],
      boring: [
        <li key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` shipped this sprint was the final piece of validating the payment flow. 
        The technical risk is now mitigated and the team can now tackle the product 
        concerns more directly.`}
        </li>,
        <li key={'52'}>
          <EmptyLink type="jira" text=" Payments server auth" />
          {` shipped this sprint means the team can focus on product concerns, 
        since the the technical risk is now eliminated.`}
        </li>,
      ]
    },
  },
  {
    id: 10,
    name: 'STR-10 API call to retrieve latest account info',
    highlight: {
      fun: [
        <span key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` will radically enhance the self-service 📱 experience for customers. 
          This priority deliverable is now done 😀.`}
        </span>,
        <span key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` vastly improves self-service 🖥️ for customers. 
          This priority deliverable is now finished 🥇.`}
        </span>,
      ],
      boring: [
        <span key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` will radically enhance the self-service experience for customers. 
          This priority deliverable is now done.`}
        </span>,
        <span key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` vastly improves self-service for customers. 
        This priority deliverable is now finished.`}
        </span>,
      ]
    },
    win: {
      fun: [
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` shipped this sprint was a major blocker for shopping cart enhancements 🛒 
        and was also causing pain for the Customer Support team ☎️ since customers didn’t 
        have self-service account access.`}
        </li>,
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` 🗣️ was finished this sprint. It unblocked many shopping cart enhancements.
         It also massively eases the incoming load 🚠 for the Customer Support 
         team since accounts are now largely self-service.`}
        </li>,
      ],
      boring: [
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` shipped this sprint was a major blocker for shopping cart enhancements 
        and was also causing pain for the Customer Support team since customers didn’t 
        have self-service account access.`}
        </li>,
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` was finished this sprint. It unblocked many shopping cart enhancements.
         It also massively eases the incoming load for the Customer Support 
         team since accounts are now largely self-service.`}
        </li>,
      ]
    },
    problem: {
      fun: [
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` exposed a number of`}
          <EmptyLink type="jira" text=" security vulnerabilities" />
          {` 🏠 that we passed on to the Security Team for review. 
        The issue is now done but we are meeting with the Security Team 
        next week to discuss any new development process updates to 
        mitigate future security bugs 🐛.`}
        </li>,
        <li key={'10'}>
          {`While working on `}
          <EmptyLink type="jira" text=" Account info API call" />
          {` 💾 exposed a number of`}
          <EmptyLink type="jira" text=" 5 security vulnerabilities" />
          {` that we passed on to the Security Team. 
        The issue is complete now but we are meeting with the Security Team 🗓️ 
        next week to improve security in our dev process.`}
        </li>,
      ],
      boring: [
        <li key={'10'}>
          <EmptyLink type="jira" text=" Account info API call" />
          {` exposed a number of`}
          <EmptyLink type="jira" text=" security vulnerabilities" />
          {` that we passed on to the Security Team for review. 
        The issue is now done but we are meeting with the Security Team 
        next week to discuss any new development process updates to 
        mitigate future security bugs.`}
        </li>,
        <li key={'10'}>
          {`While working on `}
          <EmptyLink type="jira" text=" Account info API call" />
          {` exposed a number of`}
          <EmptyLink type="jira" text=" 5 security vulnerabilities" />
          {` that we passed on to the Security Team. 
        The issue is complete now but we are meeting with the Security Team 
        next week to improve security in our dev process.`}
        </li>,
      ]
    },
  },
  {
    id: 62,
    name: 'STR-62 Dark mode default setting',
    highlight: {
      fun: [
        <span key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` 🌓  is a strategic feature to establish ourselves as a thought leader 
        in conjunction with the Marketing Team’s initiatives this quarter. 
        Per user research, we expect to 2x our leads 🤑 with this one change. 
        This critical feature was completed in this sprint.`}
        </span>,
        <span key={'62'}>
          {`Per user research, we expect to 2x our marketing leads 💰 simply 
        with the introduction of`}
          <EmptyLink type="jira" text=" Dark mode " />
          {` 🦉, which is now finished. It aligns with the Marketing Team's 
        branding initiatives this quarter.`}
        </span>,
      ],
      boring: [
        <span key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` is a strategic feature to establish ourselves as a thought leader 
        in conjunction with the Marketing Team’s initiatives this quarter. 
        Per user research, we expect to 2x our leads with this one change. 
        This critical feature was completed in this sprint.`}
        </span>,
        <span key={'62'}>
          {`Per user research, we expect to 2x our marketing leads simply 
        with the introduction of`}
          <EmptyLink type="jira" text=" Dark mode" />
          {` , which is now finished. It aligns with the Marketing Team's 
        branding initiatives this quarter.`}
        </span>,
      ]
    },
    problem: {
      fun: [
        <li key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` 🌒 is long overdue and we keep pushing it back. We didn’t start it 
      this sprint. We’re chatting 💬 with the Design team this week to 
      re-formulate our longer-term design plans.`}
        </li>,
        <li key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` is once again delayed 📅. We’re meeting with the Design team this week
         to re-group and re-prioritize dark mode and color scheme efforts 💈.`}
        </li>,
      ],
      boring: [
        <li key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` is long overdue and we keep pushing it back. We didn’t start it 
      this sprint. We’re chatting with the Design team this week to 
      re-formulate our longer-term design plans.`}
        </li>,
        <li key={'62'}>
          <EmptyLink type="jira" text=" Dark mode" />
          {` is once again delayed. We’re meeting with the Design team this week
         to re-group and re-prioritize dark mode and color scheme efforts.`}
        </li>,
      ]
    },
  },
  {
    id: 144,
    name: 'STR-144 Color scheme update',
    win: {
      fun: [
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` 🌈 directly blocked the Marketing Team’s branding work and the 
        Frontend Team’s Vue migration work. This one change in this sprint 
        helped ease a lot of pain and boosted morale 💪 in the teams.`}
        </li>,
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` was a huge headache 🤕 for the Marketing Team since they were 
        blocked with the branding initiatives without this task being done. 
        They are now unblocked. The Frontend Team 🪟 can now also continue with 
        the Vue migration work. This was a huge morale booster task.`}
        </li>,
      ],
      boring: [
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` directly blocked the Marketing Team’s branding work and the 
        Frontend Team’s Vue migration work. This one change in this sprint 
        helped ease a lot of pain and boosted morale in the teams.`}
        </li>,
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` was a huge headache for the Marketing Team since they were 
        blocked with the branding initiatives without this task being done. 
        They are now unblocked. The Frontend Team can now also continue with 
        the Vue migration work. This was a huge morale booster task.`}
        </li>,
      ]
    },
    problem: {
      fun: [
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` 🌈 was not completed for the second sprint in a row. 
        We’ve re-grouped with the Marketing Team 🖥️ to straighten out the 
        requirements and expect to finish it in the next sprint.`}
        </li>,
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` was not started for the second sprint 2️⃣ in a row. 
        We have already chatted with the Marketing Team to clean up the 
        requirements 📜 and expect to finish this task next sprint.`}
        </li>,
      ],
      boring: [
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` was not completed for the second sprint in a row. 
        We’ve re-grouped with the Marketing Team to straighten out the 
        requirements and expect to finish it in the next sprint.`}
        </li>,
        <li key={'144'}>
          <EmptyLink type="jira" text=" Color scheme update" />
          {` was not started for the second sprint in a row. 
        We have already chatted with the Marketing Team to clean up the 
        requirements and expect to finish this task next sprint.`}
        </li>,
      ]
    },
  },
  {
    id: 142,
    name: 'STR-142 Button refactor',
    problem: {
      fun: [
        <li key={'142'}>
          <EmptyLink type="jira" text=" Button refactor" />
          {` 🚧 is opening up a number of unexpected conversations with the 
        Frontend Team. We’ve documented the outstanding issues and will track 🛣️ 
        them separately and fix them in the next quarter.`}
        </li>,
        <li key={'142'}>
          <EmptyLink type="jira" text=" Button refactor" />
          {` is deceptively 😳 easy. But it's shown a lot ot tech debt we accumulated
         over the years. We're working closely with the Frontend Team to track
          these issues and will have broader plan ⏳ next week.`}
        </li>,
      ],
      boring: [
        <li key={'142'}>
          <EmptyLink type="jira" text=" Button refactor" />
          {` is opening up a number of unexpected conversations with the 
        Frontend Team. We’ve documented the outstanding issues and will track 
        them separately and fix them in the next quarter.`}
        </li>,
        <li key={'142'}>
          <EmptyLink type="jira" text=" Button refactor" />
          {` is deceptively easy. But it's shown a lot ot tech debt we accumulated
         over the years. We're working closely with the Frontend Team to track
          these issues and will have broader plan next week.`}
        </li>,
      ]
    }
  },

];

export { issues };
