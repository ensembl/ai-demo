import React, { useState } from 'react';

import {
  Box, Button, Stack, Tooltip,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

import Conversation from './shared/Conversation';
import EmptyLink from './shared/EmptyLink';
import SlackStatusReport from './SlackStatusReport';

import Sammy from './images/Sammy.png';
import Jane from './images/Jane.png';
import Bot from './images/Bot.png';

const StatusReport = () => {
  const navigate = useNavigate();
  const [buttonState, setButtonState] = useState(1);

  const [disableTeamJane, setDisableTeamJane] = useState(false);
  const [disableLaunches, setDisableLaunches] = useState(false);
  const [disableProductTeam, setDisableProductTeam] = useState(false);

  const onClick = () => {
    setButtonState(buttonState + 1);
  };

  const nextSlide = () => {
    navigate('/reminder');
  };

  const teamJane = () => {
    setDisableTeamJane(true);
  };
  const launches = () => {
    setDisableLaunches(true);
  };
  const productTeam = () => {
    setDisableProductTeam(true);
  };

  const conversations = [
    {
      id: 1,
      image: Bot,
      name: 'Acme Bot',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <span>
          {'Hey '}
          <EmptyLink text="@Sammy" />
          {`: Will the shopping cart saving feature be
          finished in time for this sprint?  I'm helping `}
          <EmptyLink text="@Jane" />
          {` pull together information for the weekly status report and this is the highest
          priority deliverable in this sprint.`}

        </span>,
    },
    {
      id: 2,
      image: Sammy,
      name: 'Sammy',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <span>
          {`We ran into some difficulty with the database sync engine.  We pulled Oli away
          from the button refactor ticket to help here. So the shopping cart saving 
          feature is still on track to be finished, but the button refactor work
          won't be done this sprint.`}
        </span>,
    },
    {
      id: 3,
      image: Bot,
      name: 'Acme Bot',
      time: '2 hours ago',
      content:
        // eslint-disable-next-line
        <span>
          {'Thanks for the update '}
          <EmptyLink text="Sammy" />
          {'. '}
          <EmptyLink text="@Jane" />
          {`: I'll be sure to include this detail when you ask me to put the status
          report together later. (I assume today.)`}
        </span>,
    },
    {
      id: 4,
      image: Jane,
      name: 'Jane',
      time: '10 minutes ago',
      content:
        // eslint-disable-next-line
        <span>
          <EmptyLink text="@Acme Bot" />
          {`: Can you generate that starter weekly status report now?  I want to 
           make sure eveything looks good before I send it out later today.`}
        </span>,
    },
    {
      id: 5,
      image: Bot,
      name: 'Acme Bot',
      time: '10 minutes ago',
      visibleToYou: true,
      content:
        // eslint-disable-next-line
        <span>
          {'I chatted with a few folks on '}
          <b>Slack</b>
          {'(and cc-ed you on those conversations).  I gathered information on '}
          <b>JIRA</b>
          {' and '}
          <b>GitHub</b>
          {`. Here's everything pulled together with a key business takeaway, 
          summary, and more details.  Feel free to edit it.  I won't be offended.`}
          <SlackStatusReport />
          <Box display="flex" justifyContent="space-between">
            <Tooltip placement="top" title={disableTeamJane ? '🤖 Posted to #TeamJane' : ''}>
              <Box>
                <Button disabled={disableTeamJane} onClick={teamJane} variant="outlined">Post report to #teamjane</Button>
              </Box>
            </Tooltip>
            <Tooltip placement="top" title={disableLaunches ? '🤖 Posted to #Launches' : ''}>
              <Box>
                <Button disabled={disableLaunches} onClick={launches} variant="outlined">Post report to #launches</Button>
              </Box>
            </Tooltip>
            <Tooltip placement="top" title={disableProductTeam ? '🤖 Posted to #Product-Team' : ''}>
              <Box>
                <Button disabled={disableProductTeam} onClick={productTeam} variant="outlined">Post report to #product-team</Button>
              </Box>
            </Tooltip>
          </Box>
        </span>,
    },
  ];
  return (
    <>
      <Stack direction="column" alignItems="center">
        {conversations.map((conversation) => (
          buttonState >= conversation.id && (
            <Conversation
              key={conversation.id}
              image={conversation.image}
              title={conversation.name}
              subheader={conversation.time}
              content={conversation.content}
              visibleToYou={conversation.visibleToYou}
            />
          )
        ))}
      </Stack>
      <Box>
        {buttonState < 5 && <Button onClick={onClick}>Next</Button>}
        {buttonState >= 5
          && (disableTeamJane || disableLaunches || disableProductTeam)
          && <Button onClick={nextSlide}>Next idea</Button>}
      </Box>

    </>
  );
};

export default StatusReport;
