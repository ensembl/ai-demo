import React from 'react';
import {
  Box, Stack,
} from '@mui/material';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  BarElement,
  LineElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

import SlackCard from './shared/SlackCard';
import EmptyLink from './shared/EmptyLink';

import Docs from './images/Docs.png';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  Tooltip,
);

const SlackStatusReport = () => {
  const statuses = [
    { status: 'On track', work: 'STR-32 Shopping cart saving' },
    { status: 'On track', work: 'STR-10 API call to retrieve latest account info' },
    { status: 'On track', work: 'STR-52 Authenticate to payments server' },
    { status: 'Finished', work: 'STR-43 Style integration block' },
    { status: 'Finished', work: 'STR-22 Password reset flow' },
    { status: 'Postponed', work: 'STR-142 Button refactor' },

  ];
  return (
    <SlackCard content={(
      <Stack>
        <Stack direction="row" style={{ margin: '13px 0', fontWeight: 'bold' }}>
          <img style={{ marginRight: '5px' }} src={Docs} height="20px" alt="slides" />
          <EmptyLink text="Team Jane weekly status report" />
        </Stack>
        <Box style={{ fontWeight: 'bold', marginRight: '10px', marginBottom: '10px' }}>
          <span>
            Key business takewaway
          </span>
        </Box>
        <Box style={{ marginBottom: '25px' }}>
          {`We're launching the new ecommerce flow next month.  This is the final
        sprint and the shopping cart saving feature is the final blocker.  It is 
        on track to be delivered.  The ecommerce luanch is on track.`}
        </Box>
        <Box style={{ fontWeight: 'bold', marginRight: '10px', marginBottom: '10px' }}>
          <span>
            Summary
          </span>
        </Box>
        <Box marginBottom="25px">
          {`Since the shopping cart feature is the highest priority and critical
          to the ecommerce launch, we pulled Oli away from the button refactor work
          to support the shopping cart feature.  The button refactor work is postponed,
          but has no impact on the launch.  Other work continues to be on track or finished
          for the current sprint.`}
        </Box>
        <Stack direction="row" justifyContent="space-between" marginBottom="25px">
          <Box>
            <div style={{ marginBottom: '10px', fontWeight: 'bold' }}>Cumulative flow diagram</div>
            <Box style={{ backgroundColor: 'white' }}>
              <Line
                options={
                  {
                    chartAreas: { backgroundColor: 'white' },
                  }
                }
                data={{
                  labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep'],
                  datasets: [
                    {
                      id: 1,
                      borderColor: 'rgba(69, 91, 230, 0.5)',
                      backgroundColor: 'rgba(69, 91, 230, 0.5)',
                      fill: 'origin',
                      data: [5, 22, 30, 50, 60],
                      animation: {
                        duration: 0,
                      },
                    },
                    {
                      id: 2,
                      borderColor: 'rgba(255, 99, 132, 0.5)',
                      backgroundColor: 'rgba(255, 99, 132, 0.5)',
                      fill: 'origin',
                      data: [10, 40, 48, 60, 73],
                      animation: {
                        duration: 0,
                      },
                    },
                  ],
                }}
              />
            </Box>
          </Box>
          <Box>
            <div style={{ fontWeight: 'bold', marginBottom: '10px' }}>Burndown</div>
            <Box style={{ backgroundColor: 'white' }}>
              <Line
                options={
                  {
                    chartAreas: { backgroundColor: 'white' },
                  }
                }
                data={{
                  labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep'],
                  datasets: [
                    {
                      id: 1,
                      borderColor: 'rgba(2, 2, 2, 0.1)',
                      backgroundColor: 'rgba(2, 2, 2, 0.1)',
                      data: [40, 30, 20, 10, 0],
                      animation: {
                        duration: 0,
                      },
                    },
                    {
                      id: 2,
                      borderColor: 'rgba(255, 99, 132, 0.5)',
                      backgroundColor: 'rgba(255, 99, 132, 0.5)',
                      data: [40, 33, 25, 5, 0],
                      animation: {
                        duration: 0,
                      },
                    },
                  ],
                }}
              />
            </Box>
          </Box>
        </Stack>
        <Stack direction="row">
          <Box style={{ width: '120px' }}>
            <div style={{ fontWeight: 'bold', marginBottom: '8px' }}>Status</div>
            <Stack>
              {
                statuses.map((status) => (
                  <Box key={status.work} marginBottom="5px">
                    {status.status === 'Postponed'
                      ? <div style={{ color: 'orange' }}>{status.status}</div>
                      : <div style={{ color: 'green' }}>{status.status}</div>}
                  </Box>
                ))
              }
            </Stack>
          </Box>
          <Box>
            <div style={{ fontWeight: 'bold', marginBottom: '8px' }}>Work</div>
            <Stack>
              {
                statuses.map((status) => (
                  <Box key={status.work} marginBottom="5px">
                    <EmptyLink type="jira" text={status.work} />
                  </Box>
                ))
              }
            </Stack>
          </Box>
        </Stack>
      </Stack>
    )}
    />
  );
};

export default SlackStatusReport;
