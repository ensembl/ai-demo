import React, { useState } from 'react';

import {
  Stack, Button, Box,
} from '@mui/material';
// import { useNavigate } from 'react-router-dom';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Line, Bar } from 'react-chartjs-2';

import Conversation from './shared/Conversation';

import EmptyLink from './shared/EmptyLink';
import SlackCard from './shared/SlackCard';

import Jane from './images/Jane.png';
import Bot from './images/Bot.png';
import Sammy from './images/Sammy.png';
import Extra from './images/Extra.png';
import Extra2 from './images/Extra2.png';
import Slides from './images/Slides.png';
import Calendar from './images/Calendar.png';
import Docs from './images/Docs.png';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
);

const StatusAndMeeting = () => {
  const [buttonState, setButtonState] = useState(1);
  const [meetingTime, setMeetingTime] = useState('Monday at 9:00am');
  const [disableMeetingButton, setDisableMeetingButton] = useState(false);

  const onNext = () => {
    setButtonState(buttonState + 1);
  };

  const setTime = (event) => {
    setMeetingTime(event.target.value);
    setDisableMeetingButton(true);
    setButtonState(buttonState + 1);
  };

  const conversations = [
    {
      id: 1,
      image: Jane,
      name: 'Jane',
      time: '11 minutes ago',
      content:
        // eslint-disable-next-line
        <span>
          {'Hey '}
          <EmptyLink text="@Acme Bot:" />
          {` The execs just asked me to give a status report.  Can you help
          me with that?`}
        </span>,
    },
    {
      id: 2,
      image: Bot,
      name: 'Acme Bot',
      time: '11 minutes ago',
      visibleToYou: true,
      content:
        // eslint-disable-next-line
        <span>
          {`😸 Glad to help!  
          I pulled together the releases from last quarter from `}
          <b>JIRA</b>
          . The upcoming roadmap from
          <b> ProductPlan</b>
          .
          {' Also grabbed some acquisition and retention stats out of '}
          <b>Amplitutde</b>
          {'.  I crafted a detailed '}
          <b>Business impact analysis</b>
          {' and '}
          <b>Executive summary</b>
          {` and put it into a new Google Slide
          deck.  Let me know if you want me to tweak it further.  Should I email 
          it to the exec team?`}
          <SlackCard content={(
            <Box>
              <Stack direction="row" style={{ margin: '13px 0', fontWeight: 'bold' }}>
                <img style={{ marginRight: '5px' }} src={Slides} height="20px" alt="slides" />
                <EmptyLink text="Q3 executive status report" />
              </Stack>
              <div style={{ fontWeight: 'bold' }}>Executive summary</div>
              <p>
                {`In the past quarter, we made significant headway in the EMEA 
              markets with the newly launched localization features.  An APAC
              launch is forthcoming in the coming quarter.`}
              </p>
              <p>
                ...
              </p>
              <div style={{ fontWeight: 'bold' }}>Business Impact Analysis</div>
              <Stack direction="row">
                <Box>
                  <Line
                    data={{
                      labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep'],
                      datasets: [
                        {
                          id: 1,
                          borderColor: 'rgba(69, 91, 230, 0.5)',
                          backgroundColor: 'rgba(69, 91, 230, 0.5)',
                          fill: 'origin',
                          data: [5, 22, 30, 50, 60],
                          animation: {
                            duration: 0,
                          },
                        },
                        {
                          id: 2,
                          borderColor: 'rgba(255, 99, 132, 0.5)',
                          backgroundColor: 'rgba(255, 99, 132, 0.5)',
                          fill: 'origin',
                          data: [10, 40, 48, 60, 73],
                          animation: {
                            duration: 0,
                          },
                        },
                      ],
                    }}
                  />
                </Box>
                <Box>
                  <Bar
                    option={{
                      animation: {
                        duration: 0,
                      },
                    }}
                    data={{
                      labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep'],
                      datasets: [
                        {
                          id: 1,
                          borderColor: 'rgba(69, 91, 230, 0.5)',
                          backgroundColor: 'rgba(69, 91, 230, 0.5)',
                          fill: 'origin',
                          data: [4000, 3000, 3492, 4229, 3329],
                          animation: {
                            duration: 0,
                          },
                        },
                        {
                          id: 2,
                          borderColor: 'rgba(255, 99, 132, 0.5)',
                          backgroundColor: 'rgba(255, 99, 132, 0.5)',
                          fill: 'origin',
                          data: [3328, 5325, 5492, 3258, 3519],
                          animation: {
                            duration: 0,
                          },
                        },
                      ],
                    }}
                  />
                </Box>
              </Stack>
            </Box>
          )}
          />
        </span>,
    },
    {
      id: 3,
      image: Jane,
      name: 'Jane',
      time: '3 minutes ago',
      content:
        // eslint-disable-next-line
        <span>
          {`Looks great! I tweaked a few things and it's ready to go.  They want
          a meeting instead of email like previously.  Can you set that up for me?`}
        </span>,
    },
    {
      id: 4,
      image: Bot,
      name: 'Acme Bot',
      time: '2 minutes ago',
      visibleToYou: true,
      content:
        // eslint-disable-next-line
        <div>
          <span>
            {`❤️ Love it. Meetings are my thing.  I whipped up a quick agenda and 
            found a few times next week that works for everyone.  Pick one
            you like or I'll just go with the first one if I don't hear from you
            in 5 minutes.`}
            <SlackCard content={(
              <Box>
                <Stack direction="row" style={{ margin: '13px 0', fontWeight: 'bold' }}>
                  <img style={{ marginRight: '5px' }} src={Docs} height="20px" alt="slides" />
                  <EmptyLink text="Meeting - Q3 executive status meeting" />
                </Stack>
                <div style={{ fontWeight: 'bold' }}>Agenda</div>
                <div>
                  <ul>
                    <li>Check-ins</li>
                    <li>Review previous releases</li>
                    <li>Talk through customer acq stats</li>
                  </ul>
                </div>
                <p>
                  ...
                </p>

              </Box>
            )}
            />
            <Stack direction="row">
              <Button
                onClick={setTime}
                disabled={disableMeetingButton}
                value="Monday at 9am"
                style={{ marginRight: '10px' }}
                variant="outlined"
              >
                Monday at 9am
              </Button>
              <Button
                onClick={setTime}
                disabled={disableMeetingButton}
                value="Tuesday at 8:30am"
                style={{ marginRight: '10px' }}
                variant="outlined"
              >
                Tuesday at 8:30am
              </Button>
              <Button
                onClick={setTime}
                disabled={disableMeetingButton}
                value="Friday at 8:30am"
                variant="outlined"
              >
                Friday at 8:30am
              </Button>
            </Stack>
          </span>
        </div>,
    },
    {
      id: 5,
      image: Bot,
      name: 'Acme Bot',
      time: '2 minutes ago',
      content:
        // eslint-disable-next-line
        <div>
          <span>
            {`Meeting scheduled in Google Calendar with the status
            report and agenda attached. `}
          </span>
          <SlackCard content={(
            <Box>
              <Stack direction="row" style={{ margin: '13px 0', fontWeight: 'bold' }}>
                <img style={{ marginRight: '5px' }} src={Calendar} height="20px" alt="slides" />
                <EmptyLink text="Q3 exec status meeting" />
              </Stack>
              <Stack>
                <div style={{ marginBottom: '10px' }}>
                  <span style={{ fontWeight: 'bold' }}>{'When: '}</span>
                  <span>{meetingTime}</span>
                </div>
                <div style={{ marginBottom: '10px' }}>
                  <span style={{ fontWeight: 'bold' }}>{'Zoom: '}</span>
                  <span><EmptyLink text="Join" /></span>
                </div>
                <div style={{ marginBottom: '10px' }}>
                  <span style={{ fontWeight: 'bold' }}>{'Where: '}</span>
                  <span>Conference room Delta Alpha</span>
                </div>
                <Stack
                  direction="row"
                  alignItems="center"
                  style={{ marginBottom: '10px' }}
                >
                  <span style={{ fontWeight: 'bold' }}>{'Guests: '}</span>
                  <span>
                    <img style={{ marginLeft: '5px', height: '25px' }} src={Jane} alt="Jane" />
                    <img style={{ marginLeft: '5px', height: '25px' }} src={Extra} alt="Extra" />
                    <img style={{ marginLeft: '5px', height: '25px' }} src={Extra2} alt="Extra2" />
                    <img style={{ marginLeft: '5px', height: '25px' }} src={Sammy} alt="Sammy" />
                  </span>
                </Stack>
              </Stack>
            </Box>
          )}
          />
        </div>,
    },
    {
      id: 6,
      image: Bot,
      name: 'Acme Bot',
      time: '2 minutes ago',
      visibleToYou: true,
      content:
        // eslint-disable-next-line
        <div>
          <span>
            {`Those early morning meetings are tough! I'll ping you again
            when we get close to see if you want `}
            <b>Doordash</b>
            {` donuts🍩 and coffee☕️ 
            to make sure the exec team isn't too grumpy.`}
          </span>
        </div>,
    },
    {
      id: 7,
      image: Jane,
      name: 'Jane',
      time: 'A moment ago',
      content:
        // eslint-disable-next-line
        <div>
          <span>
            {`Good catch. Didn't even think of that.  Thanks for all the help. 
            You're awesome!`}
          </span>
        </div>,
    },
  ];

  return (
    <>
      <Stack direction="column" alignItems="center">
        {conversations.map((conversation) => (
          buttonState >= conversation.id && (
            <Conversation
              key={conversation.id}
              image={conversation.image}
              title={conversation.name}
              subheader={conversation.time}
              content={conversation.content}
              visibleToYou={conversation.visibleToYou}
            />
          )
        ))}
      </Stack>
      <Box>
        {buttonState !== 4 && buttonState < 7 && <Button onClick={onNext}>Next</Button>}
      </Box>
    </>
  );
};

export default StatusAndMeeting;
