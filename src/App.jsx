import React, { useState } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Routes, Route,
} from 'react-router-dom';

import {
  Stack,
} from '@mui/material';

import Login from './Login';
import Reminder from './Reminder';
import BusinessImpactSummary from './BusinessImpactSummary';
import StatusReport from './StatusReport';
import StatusAndMeeting from './StatusAndMeeting';
import IssueDescription from './IssueDescription';
import OpenResponse from './OpenResponse';

const App = () => {
  const [login, setLogin] = useState(false);
  const [userName, setUserName] = useState('');

  if (!login) {
    return (<Login setLogin={setLogin} setUserName={setUserName} />);
  }
  return (
    <Stack className="App" style={{ margin: '50px', padding: '50px' }}>
      <Router>
        <Routes>
          <Route exact path="/" element={<OpenResponse userName={userName} />} />
          <Route exact path="/business-impact-summary" element={<BusinessImpactSummary />} />
          <Route exact path="/issue-description" element={<IssueDescription />} />
          <Route exact path="/status-report" element={<StatusReport />} />
          <Route exact path="/reminder" element={<Reminder />} />
          <Route exact path="/status-and-meeting" element={<StatusAndMeeting />} />
          <Route exact path="/openai" element={<BusinessImpactSummary openAI />} />
        </Routes>
      </Router>
    </Stack>
  );
};

export default App;
